## Instalacja Dockera pod Windowsem
 * pobierz instalator programu Docker Desktop https://www.docker.com/get-started
 * uruchamiamy i postępujemy zgodnie ze wskazówkami
 * możliwe, że po restarcie i uruchomieniu Docker Desktop może pojawić się alet o konieczności zaktualizowania WSL2, należy to wykonać
 
## Pobiranie projektu

### Bez git'a
 * pobieramy ten projekt jako archiwum zip
 * rozpakowyjemy pliki
 

### z git'em 
 * instalujemy program git https://git-scm.com/downloads
 * otwieramy wiersz poleceń
 * będąc w katalogu, którym chcemy mieć projekt uruchamiamy polecenie:
 
 ```
 git clone https://gitlab.com/janzaba/wordpress-docker.git
```
 
## Uruchomienie środowiska

### Pierwsze uruchomienie (razem z instalacją)
* konfiguracja znajduje się w pliku `.env` (można tam ustawić adres pod którym strona będzie dostępna lokalnie, hasła itp.)
* odpalamy dockera i instalację środowiska poleceniem:

```
docker-compose -f docker-compose.yml -f wp-auto-config.yml run --rm wp-auto-config
```

### Zwykłe uruchomienie
* aby nie instalować za każdym razem wordpressa od nowa można odpalić dockera takim poleceniem:

```
docker-compose up -d
```

### Ustawienie hosta
* w pliku `.env` w zmiennej ```WORDPRESS_WEBSITE_URL``` zapisany jest adres pod jakim strona jest dostępna,
aby komputer wiedział, że jest to adres lokalny a nie faktyczny adres www należy stworzyć przekierowanie tego hosta na 
tak zwany localhost
* aby to zrobić należy dodać wpis do pliku ```hosts``` (instrukcja jak dokonywać takich wpisów tutaj: https://wojciechpietrzak.com.pl/plik-hosts-w-systemie-windows-informacje-edycja-i-zastosowanie)
* wpis do pliku powinien wyglądać tak:

```
127.0.0.1        tobiepolsko.zhr.test
```
(zamiast ```tobiepolsko.zhr.test``` należy podać wartość zmiennej ```WORDPRESS_WEBSITE_URL```)

* jeśli wszsytko wykonano prawidłowo, to pod powyższym adresem powinna znajdować się strona www z zainstalowanym WordPressem

## Praca z plikami
* pliki wordpressa znajdują się w katalogu `/wordpress` i na nich można pracować

